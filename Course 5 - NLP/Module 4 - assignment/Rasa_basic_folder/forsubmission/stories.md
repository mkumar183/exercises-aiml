## complete path
* greet
    - utter_greet
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "delhi"}
    - slot{"location": "delhi"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Chinese"}
    - slot{"cuisine": "Chinese"}
    - utter_ask_price_range
* restaurant_search{"price_range": "Less than 300"}
    - slot{"price_range": "Less than 300"}
    - action_search_restaurants
    - slot{"location": "delhi"}
    - slot{"message": "Found: InstaBowlAddress: Hajipur, Noidaprice: 250rating: 4.8\nFound: InstaBowlAddress: Sector 11, Noidaprice: 250rating: 4.8\nFound: InstaBowlAddress: Indirapuram, Ghaziabadprice: 250rating: 4.8\nFound: Twister Cake CafeAddress: UB-35, Ground Floor, Bunglow Road, Jawahar Nagar, Kamla Nagar, New Delhiprice: 100rating: 4.7\nFound: Rosy's KitchenAddress: Shop 4, Ghai Palace, A Block Market, Preet Vihar, New Delhiprice: 250rating: 4.6\n"}
    - utter_ask_email
* email{"emailid": "mkumar183@gmail.com"}
    - action_send_email
    - slot{"location": "Delhi"}
    - utter_goodbye
* negate
    - utter_goodbye


## interactive_story_1
* greet
    - utter_greet
* restaurant_search
    - utter_ask_location
* restaurant_search{"location": "Agra"}
    - slot{"location": "Agra"}
    - utter_ask_cuisine
* restaurant_search{"cuisine": "Italian"}
    - slot{"cuisine": "Italian"}
    - utter_ask_price_range
* restaurant_search{"price_range": "between 300 and 700"}
    - slot{"price_range": "between 300 and 700"}
    - action_search_restaurants
    - slot{"location": "Agra"}
    - slot{"message": "Found: InstaBowlAddress: Hajipur, Noidaprice: 250rating: 4.8\nFound: InstaBowlAddress: Sector 11, Noidaprice: 250rating: 4.8\nFound: InstaBowlAddress: Indirapuram, Ghaziabadprice: 250rating: 4.8\nFound: Twister Cake CafeAddress: UB-35, Ground Floor, Bunglow Road, Jawahar Nagar, Kamla Nagar, New Delhiprice: 100rating: 4.7\nFound: Rosy's KitchenAddress: Shop 4, Ghai Palace, A Block Market, Preet Vihar, New Delhiprice: 250rating: 4.6\n"}
    - utter_ask_email
* email{"emailid": "mkumar183@gmail.com"}
    - action_send_email
    - slot{"location": "Delhi"}
    - utter_goodbye
* negate
    - utter_goodbye

## interactive_story_2
* restaurant_search{"cuisine": "chinese", "location": "Delhi"}
    - slot{"cuisine": "chinese"}
    - slot{"location": "Delhi"}
    - utter_ask_price_range
* restaurant_search{"price_range": "Less than 300"}
    - slot{"price_range": "Less than 300"}
    - action_search_restaurants
    - slot{"location": "Delhi"}
    - slot{"message": "Found: something something\n"}
    - utter_ask_email
* email{"emailid": "mkumar183@gmail.com"}
    - action_send_email
    - slot{"location": "Delhi"}
    - utter_goodbye
* negate
    - utter_goodbye

## interactive_story_3
* restaurant_search{"cuisine": "mexican", "location": "Delhi"}
    - slot{"cuisine": "mexican"}
    - slot{"location": "Delhi"}
    - utter_ask_price_range
* restaurant_search{"price_range": "Less than 300"}
    - slot{"price_range": "Less than 300"}
    - action_search_restaurants
    - slot{"location": "Delhi"}
    - slot{"message": "Found: InstaBowlAddress: Hajipur, Noidaprice: 250rating: 4.8\nFound: InstaBowlAddress: Sector 11, Noidaprice: 250rating: 4.8\nFound: InstaBowlAddress: Indirapuram, Ghaziabadprice: 250rating: 4.8\nFound: Twister Cake CafeAddress: UB-35, Ground Floor, Bunglow Road, Jawahar Nagar, Kamla Nagar, New Delhiprice: 100rating: 4.7\nFound: Rosy's KitchenAddress: Shop 4, Ghai Palace, A Block Market, Preet Vihar, New Delhiprice: 250rating: 4.6\n"}
    - utter_ask_email
* email{"emailid": "mkumar183@gmail.com"}
    - action_send_email
    - slot{"location": "Delhi"}
    - utter_goodbye
* negate
    - utter_goodbye

## interactive_story_4
* restaurant_search{"cuisine": "american", "location": "Agra"}
    - slot{"cuisine": "american"}
    - slot{"location": "Agra"}
    - utter_ask_price_range
* restaurant_search{"price_range": "between 300 and 700"}
    - slot{"price_range": "between 300 and 700"}
    - action_search_restaurants
    - slot{"location": "Agra"}
    - slot{"message": "Found: Annjal RestaurantAddress: Hotel Amba Inn, 1/51 Delhi Gate, Raja Mandi, Station Road, Civil Lines, Agraprice: 400rating: 4.5\nFound: Bhagat HalwaiAddress: Near Anjana Cinema, Nehru Nagar, Civil Lines, Agraprice: 300rating: 4.4\nFound: Mama Chicken Mama Franky HouseAddress: A-17, Shoping Arcade, Sadar Bazaar, Agra Cantt, Agraprice: 600rating: 4.4\nFound: The Greener SideAddress: Delhi Gate Rdprice: 300rating: 4.4\nFound: Gopaldas Pethe WaleAddress: Rajeev Plaza, M.g. Road, Opposite Shah Market, Civil Lines, Agraprice: 400rating: 4.4\n"}
    - utter_ask_email
* email{"emailid": "mkumar183@gmail.com"}
    - slot{"emailid": "mkumar183@gmail.com"}
    - action_send_email
    - slot{"location": "Agra"}
    - utter_goodbye
* negate
    - utter_goodbye

## interactive_story_5
* restaurant_search{"cuisine": "mexican", "location": "Delhi"}
    - slot{"cuisine": "mexican"}
    - slot{"location": "Delhi"}
    - utter_ask_price_range
* restaurant_search{"price_range": "More than 700"}
    - slot{"price_range": "More than 700"}
    - action_search_restaurants
    - slot{"location": "Delhi"}
    - slot{"message": "Found: Oishii WokAddress: Behind Super Mart 2, DLF Phase 4, Gurgaonprice: 700rating: 4.9\nFound: Indian AccentAddress: The Lodhi, Lodhi Road, New Delhiprice: 5000rating: 4.9\nFound: Chili's Grill & BarAddress: S 8 & 9, 2nd Floor, Pacific Mall, Tagore Garden, New Delhiprice: 1200rating: 4.9\nFound: Shanghai SurpriseAddress: DLF Phase 3, Gurgaonprice: 800rating: 4.9\nFound: Ah So YumAddress: Plot 679, Saraswati Kunj, Sector 53, Golf Course Road, Gurgaonprice: 700rating: 4.8\n"}
    - utter_ask_email
* email{"emailid": "mkumar183@gmail.com"}
    - slot{"emailid": "mkumar183@gmail.com"}
    - action_send_email
    - slot{"location": "Delhi"}
    - utter_goodbye
* negate
    - utter_goodbye
