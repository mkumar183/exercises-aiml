## intent:affirm
- yes
- yep
- yeah
- indeed
- that's right
- ok
- great
- right, thank you
- correct
- great choice
- sounds really good
- thanks
- dhanyawadam
- good morning
- good evening

## intent:email
- yes, please [abc@def.com](emailid:abc@def.com)
- abc@def.com [abc@def.com](emailid)
- yes please mkumar183@gmail.com [mkumar183@gmail.com](emailid)
- yes, mkumar183@gmail.com [mkumar183@gmail.com](emailid)
- yes please mkumar183@gmail.com [mkumar183@gmail.com](emailid)
- right [abc@def.com](emailid:abc@def.com)
- abc@def.com [abc@def.com](emailid)
- of course mkumar183@gmail.com [mkumar183@gmail.com](emailid)
- why not mkumar183@gmail.com [mkumar183@gmail.com](emailid)
- yes please mkumar183@gmail.com [mkumar183@gmail.com](emailid)



## intent:negate
- no
- No
- No thank you !
- never mind
- Never Mind
- i do not need
- no thanks !
- no need
- not required

## intent:goodbye
- bye
- goodbye
- good bye
- stop
- end
- farewell
- Bye bye
- have a good one
- okay goodbye
- thank you
- no, thank you

## intent:greet
- hey
- howdy
- hey there
- hello
- hi
- good morning
- good evening
- dear sir
- how are you
- Hi
- hola

## intent:restaurant_search
- i'm looking for a place to eat
- I want to grab lunch
- I am searching for a dinner spot
- I am looking for some restaurants in [Delhi](location).
- I am looking for some restaurants in [Bangalore](location)
- show me [chinese](cuisine) restaurants
- show me [chines](cuisine:chinese) restaurants in the [New Delhi](location:Delhi)
- show me a [mexican](cuisine) place in the [centre](location)
- i am looking for an [indian](cuisine) spot called olaolaolaolaolaola
- search for restaurants
- anywhere in the [west](location)
- I am looking for [asian fusion](cuisine) food
- I am looking a restaurant in [294328](location)
- in [Gurgaon](location)
- [South Indian](cuisine)
- [North Indian](cuisine)
- [Italian](cuisine)
- [Chinese](cuisine:chinese)
- [chinese](cuisine)
- [Lithuania](location)
- Oh, sorry, in [Italy](location)
- in [delhi](location)
- in [Jaipur](location)
- I am looking for some restaurants in [Mumbai](location)
- I am looking for [mexican indian fusion](cuisine)
- can you book a table in [rome](location) in a [moderate](price:mid) price range with [british](cuisine) food for [four](people:4) people
- [central](location) [indian](cuisine) restaurant
- please help me to find restaurants in [pune](location)
- help me find restaurant for [five](people:5) people
- please help me to find [luxury](price:high) restaurants in [pune](location)
- Please find me a restaurantin [bangalore](location)
- [mumbai](location)
- show me restaurants
- please find me [chinese](cuisine) restaurant in [delhi](location)
- can you find me a [chinese](cuisine) restaurant
- [delhi](location)
- please find me a restaurant in [ahmedabad](location)
- please show me a few [italian](cuisine) restaurants in [bangalore](location)
- find me [Chinese](cuisine) restaurant
- [New Delhi](location:Delhi)
- find me restaurnt in [mumbai](location)
- [American](cuisine)
- find me a restaurant
- [mumbai](location:Mumbai)
- [Mexican](cuisine)
- find me a restaurant in [amritsar](location)
- find a [Mexican](cuisine) restaurant in price range [Less than 300](price_range) in [Delhi](location)
- [Less than 300](price_range)
- [between 300 and 700](price_range)
- [More than 700](price_range)
- hey, can you find me a good restaurant..
- [Ahmedabad](location)
- [Agra](location)
- [700](price_range)
- find me [chinese](cuisine) restaurant in [delhi](location:Delhi)
- ['[Less than 300](price_range)']
- find me a [mexican](cuisine) restaurant in [delhi](location:Delhi)
- find me an [american](cuisine) restaurant in [Agra](location)
- ['[between 300 and 700](price_range)']
- find me a [mexican](cuisine) restaurant in new [delhi](location:Delhi)
- ['[More than 700](price_range)']


## synonym:4
- four

## synonym:5
- five

## synonym:Chennai
- madras
- chennai
- chenai

## synonym:Chinese
- chines
- chinese
- Chines

## synonym:Delhi
- New Delhi
- delhi
- Dilli
- Nayi Dilli
- new delhi

## synonym:Mumbai
- mumbai
- bombay
- bombai
- mumb

## synonym:bangalore
- Bengaluru

## synonym:chinese
- Chinese

## synonym:high
- luxury

## synonym:mid
- moderate

## synonym:vegetarian
- veggie
- vegg

## regex:emailid
- ([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})

## regex:greet
- hey[^\s]*

## regex:pincode
- [0-9]{6}

## lookup:location
- .\locations.txt

## lookup:cuisines
- Chinese
- North Indian
- South Indian
- Italian
- Mexican
- American

## lookup:price_range
- Less than 300
- between 300 and 700
- More than 700
