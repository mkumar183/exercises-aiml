{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to NumPy\n",
    "\n",
    "\n",
    "The learning objectives of this section are:\n",
    "\n",
    "* Understand advantages of vectorised code using NumPy (over standard python ways)\n",
    "* Create NumPy arrays\n",
    "    * Convert lists and tuples to NumPy arrays \n",
    "    * Create (initialise) arrays\n",
    "* Inspect the structure and content of arrays\n",
    "* Subset, slice, index and iterate through arrays\n",
    "* Compare computation times in NumPy and standard Python lists"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### NumPy Basics\n",
    "\n",
    "NumPy is a library written for scientific computing and data analysis. It stands for numerical python.\n",
    "\n",
    "The most basic object in NumPy is the ```ndarray```, or simply an ```array```, which is an **n-dimensional, homogenous** array. By homogenous, we mean that all the elements in a NumPy array have to be of the **same data type**, which is commonly numeric (float or integer). \n",
    "\n",
    "Let's see some examples of arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the numpy library\n",
    "# np is simply an alias, you may use any other alias, though np is quite standard\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2 4 5 6 7 9]\n",
      "<class 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "# Creating a 1-D array using a list\n",
    "# np.array() takes in a list or a tuple as argument, and converts into an array\n",
    "array_1d = np.array([2, 4, 5, 6, 7, 9])\n",
    "print(array_1d)\n",
    "print(type(array_1d))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[2 3 4]\n",
      " [5 8 7]]\n"
     ]
    }
   ],
   "source": [
    "# Creating a 2-D array using two lists\n",
    "array_2d = np.array([[2, 3, 4], [5, 8, 7]])\n",
    "print(array_2d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In NumPy, dimensions are called **axes**. In the 2-d array above, there are two axes, having two and three elements respectively. \n",
    "\n",
    "In NumPy terminology, for 2-D arrays:\n",
    "* ```axis = 0``` refers to the rows\n",
    "* ```axis = 1``` refers to the columns\n",
    "\n",
    "<img src=\"numpy_axes.jpg\" style=\"width: 600px; height: 400px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advantages of NumPy \n",
    "\n",
    "What is the use of arrays over lists, specifically for data analysis? Putting crudely, it is **convenience and speed **:<br>\n",
    "1. You can write **vectorised** code on numpy arrays, not on lists, which is **convenient to read and write, and concise**. \n",
    "2. Numpy is **much faster** than the standard python ways to do computations.\n",
    "\n",
    "Vectorised code typically does not contain explicit looping and indexing etc. (all of this happens behind the scenes, in precompiled C-code), and thus it is much more concise.\n",
    "\n",
    "Let's see an example of convenience, we'll see one later for speed. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Say you have two lists of numbers, and want to calculate the element-wise product. The standard python list way would need you to map a lambda function (or worse - write a ```for``` loop), whereas with NumPy, you simply multiply the arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[12, 30, 7, 35]\n"
     ]
    }
   ],
   "source": [
    "list_1 = [3, 6, 7, 5]\n",
    "list_2 = [4, 5, 1, 7]\n",
    "\n",
    "# the list way to do it: map a function to the two lists\n",
    "product_list = list(map(lambda x, y: x*y, list_1, list_2))\n",
    "print(product_list)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[12 30  7 35]\n",
      "<class 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "# The numpy array way to do it: simply multiply the two arrays\n",
    "array_1 = np.array(list_1)\n",
    "array_2 = np.array(list_2)\n",
    "\n",
    "array_3 = array_1*array_2\n",
    "print(array_3)\n",
    "print(type(array_3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the NumPy way is clearly more concise.\n",
    "\n",
    "Even simple mathematical operations on lists require for loops, unlike with arrays. For example, to calculate the square of every number in a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[9, 36, 49, 25]\n",
      "[ 9 36 49 25]\n"
     ]
    }
   ],
   "source": [
    "# Square a list\n",
    "list_squared = [i**2 for i in list_1]\n",
    "\n",
    "# Square a numpy array\n",
    "array_squared = array_1**2\n",
    "\n",
    "print(list_squared)\n",
    "print(array_squared)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This was with 1-D arrays. You'll often work with 2-D arrays (matrices), where the difference would be even greater. With lists, you'll have to store matrices as lists of lists and loop through them. With NumPy, you simply multiply the matrices."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating NumPy Arrays \n",
    "\n",
    "There are multiple ways to create numpy arrays, the most commmon ones being:\n",
    "* Convert lists or tuples to arrays using ```np.array()```, as done above\n",
    "* Initialise arrays of fixed size (when the size is known) \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2 5 6 7]\n",
      "[4 5 8 9]\n"
     ]
    }
   ],
   "source": [
    "# Convert lists or tuples to arrays using np.array()\n",
    "# Note that np.array(2, 5, 6, 7) will throw an error - you need to pass a list or a tuple\n",
    "array_from_list = np.array([2, 5, 6, 7]) \n",
    "array_from_tuple = np.array((4, 5, 8, 9))\n",
    "\n",
    "print(array_from_list)\n",
    "print(array_from_tuple)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The other common way is to initialise arrays. You do this when you know the size of the array beforehand.\n",
    "\n",
    "The following ways are commonly used:\n",
    "* ```np.ones()```: Create array of 1s\n",
    "* ```np.zeros()```: Create array of 0s\n",
    "* ```np.random.random()```: Create array of random numbers\n",
    "* ```np.arange()```: Create array with increments of a fixed step size\n",
    "* ```np.linspace()```: Create array of fixed length"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function ones in module numpy:\n",
      "\n",
      "ones(shape, dtype=None, order='C')\n",
      "    Return a new array of given shape and type, filled with ones.\n",
      "    \n",
      "    Parameters\n",
      "    ----------\n",
      "    shape : int or sequence of ints\n",
      "        Shape of the new array, e.g., ``(2, 3)`` or ``2``.\n",
      "    dtype : data-type, optional\n",
      "        The desired data-type for the array, e.g., `numpy.int8`.  Default is\n",
      "        `numpy.float64`.\n",
      "    order : {'C', 'F'}, optional, default: C\n",
      "        Whether to store multi-dimensional data in row-major\n",
      "        (C-style) or column-major (Fortran-style) order in\n",
      "        memory.\n",
      "    \n",
      "    Returns\n",
      "    -------\n",
      "    out : ndarray\n",
      "        Array of ones with the given shape, dtype, and order.\n",
      "    \n",
      "    See Also\n",
      "    --------\n",
      "    ones_like : Return an array of ones with shape and type of input.\n",
      "    empty : Return a new uninitialized array.\n",
      "    zeros : Return a new array setting values to zero.\n",
      "    full : Return a new array of given shape filled with value.\n",
      "    \n",
      "    \n",
      "    Examples\n",
      "    --------\n",
      "    >>> np.ones(5)\n",
      "    array([ 1.,  1.,  1.,  1.,  1.])\n",
      "    \n",
      "    >>> np.ones((5,), dtype=int)\n",
      "    array([1, 1, 1, 1, 1])\n",
      "    \n",
      "    >>> np.ones((2, 1))\n",
      "    array([[ 1.],\n",
      "           [ 1.]])\n",
      "    \n",
      "    >>> s = (2,2)\n",
      "    >>> np.ones(s)\n",
      "    array([[ 1.,  1.],\n",
      "           [ 1.,  1.]])\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# Tip: Use help to see the syntax when required\n",
    "help(np.ones)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1., 1., 1.],\n",
       "       [1., 1., 1.],\n",
       "       [1., 1., 1.],\n",
       "       [1., 1., 1.],\n",
       "       [1., 1., 1.]])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Creating a 5 x 3 array of ones\n",
    "np.ones((5, 3))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1, 1, 1],\n",
       "       [1, 1, 1],\n",
       "       [1, 1, 1],\n",
       "       [1, 1, 1],\n",
       "       [1, 1, 1]])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Notice that, by default, numpy creates data type = float64\n",
    "# Can provide dtype explicitly using dtype\n",
    "np.ones((5, 3), dtype = np.int)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0, 0, 0, 0])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Creating array of zeros\n",
    "np.zeros(4, dtype = np.int)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.23107612, 0.08340714, 0.3628846 , 0.19075772],\n",
       "       [0.96846042, 0.59549322, 0.83236082, 0.84578352],\n",
       "       [0.81253895, 0.77468786, 0.48086988, 0.22354705]])"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Array of random numbers\n",
    "np.random.random([3, 4])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95]\n"
     ]
    }
   ],
   "source": [
    "# np.arange()\n",
    "# np.arange() is the numpy equivalent of range()\n",
    "# Notice that 10 is included, 100 is not, as in standard python lists\n",
    "\n",
    "# From 10 to 100 with a step of 5\n",
    "numbers = np.arange(10, 100, 5)\n",
    "print(numbers)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([15.   , 15.125, 15.25 , 15.375, 15.5  , 15.625, 15.75 , 15.875,\n",
       "       16.   , 16.125, 16.25 , 16.375, 16.5  , 16.625, 16.75 , 16.875,\n",
       "       17.   , 17.125, 17.25 , 17.375, 17.5  , 17.625, 17.75 , 17.875,\n",
       "       18.   ])"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# np.linspace()\n",
    "# Sometimes, you know the length of the array, not the step size\n",
    "\n",
    "# Array of length 25 between 15 and 18\n",
    "np.linspace(15, 18, 25)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apart from the methods mentioned above, there are a few more NumPy functions that you can use to create special NumPy arrays:\n",
    "\n",
    "-  `np.full()`: Create a constant array of any number ‘n’\n",
    "-  `np.tile()`: Create a new array by repeating an existing array for a particular number of times\n",
    "-  `np.eye()`: Create an identity matrix of any dimension\n",
    "-  `np.randint()`: Create a random array of integers within a particular range"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[7, 7, 7],\n",
       "       [7, 7, 7],\n",
       "       [7, 7, 7],\n",
       "       [7, 7, 7]])"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Creating a 4 x 3 array of 7s using np.full()\n",
    "# The default data type here is int only\n",
    "np.full((4,3), 7)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0, 1, 2, 0, 1, 2, 0, 1, 2])"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Given an array, np.tile() creates a new array by repeating the given array for any number of times that you want\n",
    "# The default data type her is int only\n",
    "arr = ([0, 1, 2])\n",
    "np.tile(arr, 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0, 1, 2, 0, 1, 2],\n",
       "       [0, 1, 2, 0, 1, 2],\n",
       "       [0, 1, 2, 0, 1, 2]])"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# You can also create multidimensional arrays using np.tile()\n",
    "np.tile(arr, (3,2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1, 0, 0, 0],\n",
       "       [0, 1, 0, 0],\n",
       "       [0, 0, 1, 0],\n",
       "       [0, 0, 0, 1]])"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Create a 3 x 3 identity matrix using np.eye()\n",
    "# The default data type here is float. So if we want integer values, we need to specify the dtype to be int\n",
    "np.eye(4, dtype = int)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[3, 6, 5, 5],\n",
       "       [1, 6, 1, 7],\n",
       "       [2, 8, 1, 6],\n",
       "       [4, 2, 2, 3]])"
      ]
     },
     "execution_count": 39,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Create a 4 x 4 random array of integers ranging from 0 to 9\n",
    "np.random.randint(0, 10, (4,4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inspect the Structure and Content of Arrays\n",
    "\n",
    "It is helpful to inspect the structure of numpy arrays, especially while working with large arrays. Some attributes of numpy arrays are:\n",
    "* ```shape```: Shape of array (n x m)\n",
    "* ```dtype```: data type (int, float etc.)\n",
    "* ```ndim```: Number of dimensions (or axes)\n",
    "* ```itemsize```: Memory used by each array elememnt in bytes\n",
    "\n",
    "\n",
    "Let's say you are working with a moderately large array of size 1000 x 300. First, you would want to wrap your head around the basic shape and size of the array. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0.92154133 0.56135693 0.52491735 0.49140154 0.09869174 0.50736765\n",
      " 0.51947471 0.65824166 0.39178264 0.066841   0.33389929 0.88497032\n",
      " 0.5687252  0.24745712 0.21211227 0.28251644 0.48682752 0.56391734\n",
      " 0.47658767 0.08930232 0.38485269 0.50224028 0.40811823 0.94545938\n",
      " 0.33648284 0.39923854 0.5605831  0.68755961 0.93319745 0.26843903\n",
      " 0.25837203 0.12113041 0.191291   0.78296963 0.52309393 0.46218581\n",
      " 0.18731435 0.20444469 0.953399   0.3761728  0.46996531 0.49612568\n",
      " 0.31163366 0.860348   0.19783443 0.93728921 0.19232698 0.15820277\n",
      " 0.17388962 0.79793826 0.64184171 0.05314474 0.37774084 0.5823105\n",
      " 0.09229187 0.18324785 0.70644805 0.49463547 0.88091696 0.1970187\n",
      " 0.72303752 0.08346261 0.67047942 0.16930017 0.0335998  0.29569895\n",
      " 0.17973418 0.90627617 0.36803376 0.47024341 0.79922629 0.59929268\n",
      " 0.49697537 0.59185196 0.74206518 0.46266118 0.29990308 0.78755971\n",
      " 0.24475285 0.04260712 0.03770968 0.79208092 0.10238573 0.1973653\n",
      " 0.57989612 0.17037134 0.29616239 0.75258666 0.10709343 0.1237809\n",
      " 0.7593341  0.02630475 0.58368871 0.39720301 0.98345463 0.45890933\n",
      " 0.31339654 0.07980646 0.83312004 0.45296441 0.84714032 0.04251219\n",
      " 0.41394924 0.72405516 0.56611749 0.28105381 0.36194633 0.57613717\n",
      " 0.7983875  0.53925219 0.12782147 0.52524172 0.72703077 0.65027941\n",
      " 0.20130336 0.08870859 0.86890397 0.55134998 0.80022812 0.66333695\n",
      " 0.43449802 0.96815171 0.66387919 0.80805362 0.76970558 0.54146134\n",
      " 0.08290205 0.3386335  0.49459871 0.41150499 0.969126   0.86956799\n",
      " 0.21222    0.6232591  0.45490896 0.91630146 0.31392072 0.22237361\n",
      " 0.69898471 0.06958471 0.76705665 0.98765014 0.66731855 0.78042054\n",
      " 0.58929383 0.96176594 0.87271288 0.24435897 0.48787866 0.27527346\n",
      " 0.35773567 0.32591341 0.23150771 0.99374488 0.39475986 0.22538583\n",
      " 0.78125187 0.7493611  0.29817352 0.96883232 0.06070452 0.4959385\n",
      " 0.45002236 0.21336573 0.08436702 0.52965416 0.42502553 0.36225382\n",
      " 0.23124889 0.95236769 0.29061273 0.74331822 0.55804285 0.39727461\n",
      " 0.84682387 0.16194023 0.70878064 0.4198265  0.40936288 0.13199342\n",
      " 0.44525908 0.46381706 0.24520264 0.27256324 0.29458546 0.90493881\n",
      " 0.92491304 0.89691587 0.15392366 0.73731871 0.63975138 0.51196263\n",
      " 0.03560087 0.49622813 0.07140764 0.40876683 0.77371938 0.65350397\n",
      " 0.85806899 0.46801183 0.25488599 0.89524782 0.49164328 0.0435614\n",
      " 0.92665464 0.75270066 0.82818542 0.76951414 0.85206234 0.81401598\n",
      " 0.80732077 0.00808358 0.06041994 0.02208056 0.00603721 0.09567094\n",
      " 0.75753862 0.56674322 0.17933472 0.81905037 0.00388679 0.21513141\n",
      " 0.35706042 0.42549847 0.69884963 0.5297487  0.18851529 0.44797521\n",
      " 0.89548024 0.15770208 0.68114374 0.33550187 0.12394969 0.39217263\n",
      " 0.34526921 0.38513626 0.07952764 0.92401373 0.94484527 0.6285441\n",
      " 0.71484102 0.18441255 0.45020306 0.96707993 0.55799282 0.6147062\n",
      " 0.40308088 0.61889409 0.72048126 0.67406832 0.12696822 0.53422537\n",
      " 0.09976877 0.45319991 0.72332837 0.08064353 0.82467884 0.06340518\n",
      " 0.11614663 0.10886342 0.18242508 0.29165395 0.1653535  0.79468849\n",
      " 0.58573658 0.16903836 0.66020538 0.64269973 0.138425   0.80803833\n",
      " 0.45284676 0.98338881 0.16218995 0.43573038 0.55968272 0.4545782\n",
      " 0.9819964  0.82326658 0.26092005 0.75690602 0.27877036 0.13383921\n",
      " 0.77309125 0.50430894 0.90454381 0.20213217 0.62852264 0.21679671\n",
      " 0.95286063 0.47122282 0.920113   0.16813852 0.94034679 0.35232384\n",
      " 0.24658295 0.96898522 0.48886211 0.81030018 0.68859742 0.8341043 ]\n"
     ]
    }
   ],
   "source": [
    "# Initialising a random 1000 x 300 array\n",
    "rand_array = np.random.random((1000, 300))\n",
    "\n",
    "# Print the second row\n",
    "print(rand_array[999, ])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Shape: (1000, 300)\n",
      "dtype: float64\n",
      "Dimensions: 2\n",
      "Item size: 8\n",
      "(1000, 300)\n",
      "float64\n",
      "2\n",
      "8\n"
     ]
    }
   ],
   "source": [
    "# Inspecting shape, dtype, ndim and itemsize\n",
    "print(\"Shape: {}\".format(rand_array.shape))\n",
    "print(\"dtype: {}\".format(rand_array.dtype))\n",
    "print(\"Dimensions: {}\".format(rand_array.ndim))\n",
    "print(\"Item size: {}\".format(rand_array.itemsize))\n",
    "\n",
    "print(rand_array.shape)\n",
    "print(rand_array.dtype)\n",
    "print(rand_array.ndim)\n",
    "print(rand_array.itemsize)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Reading 3-D arrays is not very obvious, because we can only print maximum two dimensions on paper, and thus they are printed according to a specific convention. Printing higher dimensional arrays follows the following conventions:\n",
    "* The last axis is printed from left to right\n",
    "* The second-to-last axis is printed from top to bottom\n",
    "* The other axes are also printed top-to-bottom, with each slice separated by another using an empty line \n",
    "\n",
    "Let's see some examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[[ 0  1  2  3]\n",
      "  [ 4  5  6  7]\n",
      "  [ 8  9 10 11]]\n",
      "\n",
      " [[12 13 14 15]\n",
      "  [16 17 18 19]\n",
      "  [20 21 22 23]]]\n"
     ]
    }
   ],
   "source": [
    "# Creating a 3-D array\n",
    "# reshape() simply reshapes a 1-D array \n",
    "array_3d = np.arange(24).reshape(2, 3, 4)\n",
    "print(array_3d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The last axis has 4 elements, and is printed from left to right.\n",
    "* The second last has 3, and is printed top to bottom\n",
    "* The other axis has 2, and is printed in the two separated blocks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subset, Slice, Index and Iterate through Arrays\n",
    "\n",
    "For **one-dimensional arrays**, indexing, slicing etc. is **similar to python lists** - indexing starts at 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0 1 2 3 4 5 6 7 8 9]\n"
     ]
    }
   ],
   "source": [
    "# Indexing and slicing one dimensional arrays\n",
    "array_1d = np.arange(10)\n",
    "print(array_1d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2\n",
      "[2 5 6]\n",
      "[2 3 4 5 6 7 8 9]\n",
      "[0 1 2]\n",
      "[2 3 4 5 6]\n",
      "[0 2 4 6 8]\n"
     ]
    }
   ],
   "source": [
    "# Third element\n",
    "print(array_1d[2])\n",
    "\n",
    "# Specific elements\n",
    "# Notice that array[2, 5, 6] will throw an error, you need to provide the indices as a list\n",
    "print(array_1d[[2, 5, 6]])\n",
    "\n",
    "# Slice third element onwards\n",
    "print(array_1d[2:])\n",
    "\n",
    "# Slice first three elements\n",
    "print(array_1d[:3])\n",
    "\n",
    "# Slice third to seventh elements\n",
    "print(array_1d[2:7])\n",
    "\n",
    "# Subset starting 0 at increment of 2 \n",
    "print(array_1d[0::2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "4\n",
      "9\n",
      "16\n",
      "25\n",
      "36\n",
      "49\n",
      "64\n",
      "81\n"
     ]
    }
   ],
   "source": [
    "# Iterations are also similar to lists\n",
    "for i in array_1d:\n",
    "    print(i**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Multidimensional arrays** are indexed using as many indices as the number of dimensions or axes. For instance, to index a 2-D array, you need two indices - ```array[x, y]```. \n",
    "\n",
    "Each axes has an index starting at 0. The following figure shows the axes and their indices for a 2-D array.\n",
    "\n",
    "<img src=\"2_d_array.png\" style=\"width: 350px; height: 300px\">\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 2  5  7  5]\n",
      " [ 4  6  8 10]\n",
      " [10 12 15 19]]\n"
     ]
    }
   ],
   "source": [
    "# Creating a 2-D array\n",
    "array_2d = np.array([[2, 5, 7, 5], [4, 6, 8, 10], [10, 12, 15, 19]])\n",
    "print(array_2d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "12\n"
     ]
    }
   ],
   "source": [
    "# Third row second column\n",
    "print(array_2d[2, 1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 4  6  8 10]\n",
      "<class 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "# Slicing the second row, and all columns\n",
    "# Notice that the resultant is itself a 1-D array\n",
    "print(array_2d[1, :])\n",
    "print(type(array_2d[1, :]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 7  8 15]\n"
     ]
    }
   ],
   "source": [
    "# Slicing all rows and the third column\n",
    "print(array_2d[:, 2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 2  5  7]\n",
      " [ 4  6  8]\n",
      " [10 12 15]]\n"
     ]
    }
   ],
   "source": [
    "# Slicing all rows and the first three columns\n",
    "print(array_2d[:, :3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Iterating on 2-D arrays** is done with respect to the first axis (which is row, the second axis is column). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2 5 7 5]\n",
      "[ 4  6  8 10]\n",
      "[10 12 15 19]\n"
     ]
    }
   ],
   "source": [
    "# Iterating over 2-D arrays\n",
    "for row in array_2d:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[[ 0  1  2  3]\n",
      "  [ 4  5  6  7]\n",
      "  [ 8  9 10 11]]\n",
      "\n",
      " [[12 13 14 15]\n",
      "  [16 17 18 19]\n",
      "  [20 21 22 23]]]\n"
     ]
    }
   ],
   "source": [
    "# Iterating over 3-D arrays: Done with respect to the first axis\n",
    "array_3d = np.arange(24).reshape(2, 3, 4)\n",
    "print(array_3d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0  1  2  3]\n",
      " [ 4  5  6  7]\n",
      " [ 8  9 10 11]]\n",
      "[[12 13 14 15]\n",
      " [16 17 18 19]\n",
      " [20 21 22 23]]\n"
     ]
    }
   ],
   "source": [
    "# Prints the two blocks\n",
    "for row in array_3d:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compare Computation Times in NumPy and Standard Python Lists\n",
    "\n",
    "We mentioned that the key advantages of numpy are convenience and speed of computation. \n",
    "\n",
    "You'll often work with extremely large datasets, and thus it is important point for you to understand how much computation time (and memory) you can save using numpy, compared to standard python lists.   \n",
    "\n",
    "Let's compare the computation times of arrays and lists for a simple task of calculating the element-wise product of numbers. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.21300029754638672\n",
      "0.007015228271484375\n",
      "The ratio of time taken is 30.362561174551388\n"
     ]
    }
   ],
   "source": [
    "## Comparing time taken for computation\n",
    "list_1 = [i for i in range(1000000)]\n",
    "list_2 = [j**2 for j in range(1000000)]\n",
    "\n",
    "# list multiplication\n",
    "import time\n",
    "\n",
    "# store start time, time after computation, and take the difference\n",
    "t0 = time.time()\n",
    "product_list = list(map(lambda x, y: x*y, list_1, list_2))\n",
    "t1 = time.time()\n",
    "list_time = t1 - t0 \n",
    "print(t1-t0)\n",
    "\n",
    "\n",
    "# numpy array \n",
    "array_1 = np.array(list_1)\n",
    "array_2 = np.array(list_2)\n",
    "\n",
    "t0 = time.time()\n",
    "array_3 = array_1*array_2\n",
    "t1 = time.time()\n",
    "numpy_time = t1 - t0\n",
    "\n",
    "print(t1-t0)\n",
    "\n",
    "print(\"The ratio of time taken is {}\".format(list_time/numpy_time))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[  1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12],\n",
       "       [ 13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24],\n",
       "       [ 25,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36],\n",
       "       [ 37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48],\n",
       "       [ 49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60],\n",
       "       [ 61,  62,  63,  64,  65,  66,  67,  68,  69,  70,  71,  72],\n",
       "       [ 73,  74,  75,  76,  77,  78,  79,  80,  81,  82,  83,  84],\n",
       "       [ 85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,  96],\n",
       "       [ 97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108],\n",
       "       [109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120],\n",
       "       [121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132]])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "a = np.array(range(1,11*12+1)).reshape(11,12)\n",
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[5 7 0] [8 2 4]\n",
      "[8 2 4] [5 7 0]\n"
     ]
    }
   ],
   "source": [
    "#swap nth and mth row\n",
    "import numpy as np\n",
    "a = np.array([[4, 3, 1], \n",
    "     [5, 7, 0], \n",
    "     [9, 9, 3], \n",
    "     [8, 2, 4]]) \n",
    "\n",
    "def swap(a,m,n):\n",
    "    x = list(a[n,])\n",
    "    a[n,] = a[m,]\n",
    "    a[m,] = np.array(x)\n",
    "    #print(a[m,], a[n,])\n",
    "\n",
    "swap(a,1,3)\n",
    "print(a[1,], a[3,])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 1 1 1]\n",
      " [1 0 0 1]\n",
      " [1 0 0 1]\n",
      " [1 1 1 1]]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "array([[1, 1, 1],\n",
       "       [1, 0, 0],\n",
       "       [1, 0, 0],\n",
       "       [1, 1, 1]])"
      ]
     },
     "execution_count": 44,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "#n*n array with 1s on border \n",
    "\n",
    "n = 4\n",
    "a = np.zeros(n*n, dtype=int).reshape(n,n)\n",
    "a[[0,n-1],] = 1\n",
    "a[:,[0,n-1]] = 1\n",
    "print(a)\n",
    "a[ : , :3]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, numpy is **an order of magnitude faster** than lists. This is with arrays of size in millions, but you may work on much larger arrays of sizes in order of billions. Then, the difference is even larger.\n",
    "\n",
    "Some reasons for such difference in speed are:\n",
    "* NumPy is written in C, which is basically being executed behind the scenes\n",
    "* NumPy arrays are more compact than lists, i.e. they take much lesser storage space than lists\n",
    "\n",
    "\n",
    "The following discussions demonstrate the differences in speeds of NumPy and standard python:\n",
    "1. https://stackoverflow.com/questions/8385602/why-are-numpy-arrays-so-fast\n",
    "2. https://stackoverflow.com/questions/993984/why-numpy-instead-of-python-lists\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
