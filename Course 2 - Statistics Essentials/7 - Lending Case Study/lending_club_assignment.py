import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

INT_RATE_RANGE = pd.Series(np.arange(0.0, 0.33, 0.03))
REVOL_UTIL_RANGE = pd.Series([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
REVOL_BAL_RANGE = pd.Series(np.arange(0, 150000, 10000))
DTI_RANGE = pd.Series(np.arange(0, 31, 1))
TOTAL_ACC_RANGE = pd.Series(np.arange(0, 100, 5))


# Reads loan data in pandas data frame
def read_loan_data():
    loan_df = pd.read_csv('loan.csv', index_col=None, na_values=['NA'], sep=',', low_memory=False)
    print_df(loan_df.describe())
    print_df(loan_df.head(5))
    print(len(loan_df.columns))
    # There are about 40K (39717) records in each column in total in the loan data overall and 111 columns
    # Our aim is to explore if there is a relation between loan default which is represented by `loan_status` column
    # and the data we have.
    return loan_df


# Cleans loan data frame
def clean_loan_data(loan_df):
    # Number of nulls in each column
    print_df(loan_df.isnull().sum())
    # 1. Remove all columns with 100 % null values
    loan_df.dropna(how='all', axis='columns', inplace=True)
    print_df(loan_df.isnull().sum())
    print(len(loan_df.columns))
    # Number of columns left after remove completely null columns is 57
    # We now analyze on these 57 columns
    print(loan_df.info())
    c1 = list(loan_df.columns)
    # 2. Remove all columns with only zero values
    loan_df = loan_df.loc[:, (loan_df != 0).any(axis=0)]
    print(loan_df.info())
    # The columns that got removed are acc_now_delinq and delinq_amnt
    c2 = list(loan_df.columns)
    print("The columns that got removed are: ", list(set(c1) - set(c2)))
    # Get number of zero rows in each column
    print_df((loan_df == 0).sum())
    # Let us find the percent defaults in data
    print(round(len(loan_df.loc[loan_df['loan_status'] == 'Charged Off'])/len(loan_df['id']) * 100, 2))
    # 14.17 % is the default which is about 5627 loan applications
    # Lets remove all columns with 90% which also have 90% null values for the defaulted applications
    # i.e. more than 5345 columns with null or 0 values in above data
    print('% null values in each column')
    print(round(loan_df.isnull().sum()/len(loan_df['id'])*100))
    print('% null values in each column with defaulted loans')
    print(round(loan_df.loc[loan_df['loan_status'] == 'Charged Off'].isnull().sum()/5627*100))
    # next_pymnt_d has 97% null values and 100% null values among defaulted loans hence remove it
    loan_df = loan_df.drop('next_pymnt_d', axis=1)
    # months since last record also looks like a suitable candidate to delete since null values is 93% and 90%
    # Before deleting lets first find out the same for 0 values.
    print('% 0 values in each column')
    print(round((loan_df == 0).sum() / len(loan_df['id']) * 100))
    # Remove all completely 0 value columns
    loan_df = loan_df.drop(['collections_12_mths_ex_med', 'chargeoff_within_12_mths', 'tax_liens'], axis=1)
    print(round((loan_df == 0).sum() / len(loan_df['id']) * 100))
    # Percentage 0 values in all columns
    # pub_rec(95), out_prncp(97), out_prncp_inv(97), total_rec_late_fee(95),
    # collection_recovery_fee(90), pub_rec_bankruptcies(94), delinq_2yrs(89), recoveries(89)
    print('% 0 values in each column with defaulted loans')
    print(round((loan_df.loc[loan_df['loan_status'] == 'Charged Off'] == 0).sum() / 5627 * 100))
    # Percentage 0 values in columns with defaulted loan
    # delinq_2yrs(88), pub_rec(92), out_prncp(100), out_prncp_inv(100), total_rec_late_fee(85), pub_rec_bankruptcies(95)
    # Let us drop only columns we are totally sure wouldn't contribute towards finding default relationship with data
    #  out_prncp & out_prncp_inv can be removed
    loan_df = loan_df.drop(['out_prncp', 'out_prncp_inv', 'pub_rec_bankruptcies'], axis=1)

    # Since we don't have any other significant columns with these values let us remove only mths_since_last_record
    loan_df = loan_df.drop('mths_since_last_record', axis=1)
    # Columns after removing more than 90% null values, now let us look for columns that we may never use
    # pymnt_plan column have all 'n' values, initial_list_status similarly have all 'f' values,
    # application_type have all 'INDIVIDUAL' values, and policy_code = 1 values
    loan_df = loan_df.drop(['pymnt_plan', 'initial_list_status', 'application_type', 'policy_code'], axis=1)

    # Subgrade contains grade as well so grade is an extra column
    loan_df = loan_df.drop(['grade'], axis=1)

    # DERIVE abs_grade an ordinal value based on the sub_grade also grade is not useful
    grades = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
    sub_grade_max = 5
    grades_dict = dict()
    index = 1
    for grade in grades:
        for sub_grade in range(1, sub_grade_max+1):
            grades_dict[grade+str(sub_grade)] = index
            index += 1
    loan_df.insert(list(loan_df.columns).index('sub_grade') + 1, 'abs_grade',
                   loan_df['sub_grade'].apply(lambda g: grades_dict[g]))

    # For further analysis let us now store cleaned data in a file and use this from now on
    # We may need to further clean this data
    print(loan_df.info())
    # This new data has 43 columns we need to analyze.

    loan_df.to_csv('loan_cleaned.csv', sep=',')


# Reads loan data in pandas data frame
def read_loan_clean_data():
    loan_df = pd.read_csv('loan_cleaned.csv', index_col=None, na_values=['NA'], sep=',', low_memory=False)
    print_df(loan_df.describe())
    print_df(loan_df.head(5))
    print(len(loan_df.columns))
    print('Null values: ')
    print_df(loan_df.isnull().sum())
    print_df((loan_df == 0).sum())
    # We do not need to remove rows with zero values as of now
    return loan_df


def process_data(loan_df):
    print('process_data')
    # convert percentage column to float
    loan_df['int_rate'] = loan_df['int_rate'].apply(lambda percent: round(float(percent.strip('%'))/100, 2))
    loan_df['revol_util'] = loan_df['revol_util'].apply(
        lambda percent: round(float(percent.strip('%'))/100, 2) if percent is not np.nan else percent)
    loan_df['revol_util'] = loan_df['revol_util'].apply(replace_values_revol_util_rate)
    loan_df['revol_bal'] = loan_df['revol_bal'].apply(replace_values_revol_bal)
    print(loan_df['revol_util'].unique())
    # Replace emp title null with UNK unknown, convert to upper case, and trim
    loan_df.loc[pd.isnull(loan_df['emp_title']), ['emp_title']] = 'UNK'
    loan_df['emp_title'] = loan_df['emp_title'].apply(lambda et: et.upper().replace(' ', '')
                                                      .replace("'", '').replace('-', ''))
    loan_df['emp_length'] = loan_df['emp_length'].apply(replace_values_emp_length)
    loan_df['int_rate_interval'] = loan_df['int_rate'].apply(replace_values_int_rate)
    loan_df['dti'] = loan_df['dti'].apply(replace_values_dti)
    loan_df['total_acc'] = loan_df['total_acc'].apply(replace_values_total_acc)
    return loan_df


def analyze_univariate(loan_df):
    print('-------------Univariate analysis--------------')
    print(loan_df.shape)
    print_df(loan_df.describe())
    # 1. Average Loan amount is 11,219
    # 2. Average installment is 324 with 50% having to pay installment more than 280 and 75% having to pay more than 430
    # 3. Average annual income is 68,969 with 50% having salary below 59,000 and 25% below salary 40,000
    # 4. Average dti is 13.32% with deviation of 6.68% which is substantial. This means debt income ratio varies a
    # substantial amount.
    # 5. Revolving credit balance average is about 13,383 while deviation is 15,885 again substantial,
    # Interestingly, about 25% have a credit revolving balance greater than 17,058
    # and max is about 150K. This might prove useful.
    # 6. total_acc or Number of credit lines average is 22 with deviation of 11. Minimum credit lines is 2 which means
    # every borrower has atleast 2 credit lines. About 50% have more than 20 credit lines.
    # 7. Average interest rate is about 12% with about 25% people paying more than 15% interest.

    # Now let us analyze categorical variables
    print('Loan Status value counts : ', loan_df['loan_status'].value_counts())
    # Fully paid is 32950(82.96%), Charged Off or Defaulted loans is 5627(14.17%), and Current or ongoing is 1140(2.87%)
    print('Loan Term value counts :', loan_df['term'].value_counts())
    # 29096 (73.26%) loans are of 36 months term while 10621(26.74%) loans are 60 months
    print('Loan absolute grade value counts :', loan_df['abs_grade'].value_counts())

    group_by_abs_grade = loan_df.groupby('abs_grade').size().reset_index()
    group_by_abs_grade.columns = ['abs_grade', 'number_of_loans']
    group_by_abs_grade_for_bad_loan = (loan_df.loc[loan_df['loan_status'] == 'Charged Off']).groupby('abs_grade')\
        .size().reset_index()
    group_by_abs_grade_for_bad_loan.columns = ['abs_grade', 'number_of_bad_loans']
    abs_grade_wise = pd.merge(group_by_abs_grade, group_by_abs_grade_for_bad_loan, how='inner', on='abs_grade')
    abs_grade_wise['ratio_bad_loans'] = abs_grade_wise['number_of_bad_loans'] / abs_grade_wise['number_of_loans']

    # Plot frequency plot of absolute grade with and without no loan defaults.
    plt.subplot(3, 1, 1)
    plt.title('Number of loans & Number of bad loans vs Absolute grades', y=1.1)
    sns.countplot(x='abs_grade', data=loan_df, order=sorted(list(loan_df['abs_grade'].unique())))
    plt.subplot(3, 1, 2)
    sns.barplot(x='abs_grade', y='number_of_bad_loans', data=group_by_abs_grade_for_bad_loan)
    plt.subplot(3, 1, 3)
    sns.barplot(x='abs_grade', y='ratio_bad_loans', data=abs_grade_wise)
    plt.show()
    # The plot clearly shows that the ratio of bad loans increase with absolute grade.
    # Also it is particularly high for grade = 30 which is F5. No conclusions here though.

    # Let us now see if we can find anything related to employee title:
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='emp_title', titles=['emp_title', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_title', number_of_loans_threshold=5,
                            plt_ratio_threshold=0.25, xtick_label=4)
    # As evident from plot, for some employee titles/companies like Level 3 communications the ratio of bad loans is
    # about 65% which is quite significant increase from average bad loans which is about 14.17%
    # We have plotted against all employee titles/companies for whom ratio of bad loans is above 25%
    # WellsFargo Mortgage, Paychecxinc, and GeneralAtomics have % bad loans above 50%
    # Maybe these companies have higher layoffs or lower salaries

    # Let us now see if we can find anything related to states:
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='addr_state', titles=['addr_state', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_addr_state', number_of_loans_threshold=2,
                            plt_ratio_threshold=0.15, xtick_label=8)
    # Address state NE has 60% default loans

    group_by_analysis_ratio(loan_df=loan_df, group_by_col='int_rate_interval', titles=['int_rate_interval',
                                                                                       'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_int_rate', number_of_loans_threshold=2,
                            plt_ratio_threshold=0.15, xtick_label=8)
    # Again we can see the default ratio increases with increase in interest rate.

    # Analyze employee duration of work in the company
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='emp_length', titles=['emp_length', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_emp_length', number_of_loans_threshold=2,
                            plt_ratio_threshold=0.1, xtick_label=8)
    # Number of years worked in the company does not impact default ratio by the result since
    # the default ratio is almost 14% which is the default percentage overall.

    # Analyze revolving line utilization rate
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='revol_util', titles=['revol_util', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_revol_util', number_of_loans_threshold=2,
                            plt_ratio_threshold=0.1, xtick_label=8)
    # We see that the default loan ratio increases with Revolving line utilization rate but
    # the ratio of default is about 20% between 90%-100% which is significant.

    # Analyze total credit revolving balance
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='revol_bal', titles=['revol_bal', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_revol_bal', number_of_loans_threshold=2,
                            plt_ratio_threshold=0.1, xtick_label=8)
    # We see that the total credit revolving balance doesn't show any pattern nor is significant
    # compared to 14% total default percentage.

    # Analyze by zip_code
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='zip_code', titles=['zip_code', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_zip_code', number_of_loans_threshold=20,
                            plt_ratio_threshold=0.25, xtick_label=8)
    # 561xx and 746xx are two zip-codes with significant delinquency of about 70%.
    # 5 of 7 loans are bad in 561xx also same for 746xx
    # Given our minimum loan threshold more than  out of 5 people from these zipcodes are delinquent

    # Ratio of debt obligation excluding mortgage and loan by LC and monthly income
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='dti', titles=['dti', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_dti', number_of_loans_threshold=30,
                            plt_ratio_threshold=0.1, xtick_label=8)
    # dti too doesn't seem to have a pattern or significant impact. Although looks like a skewed normal distribution
    # with mean at 25.

    # Credit lines analysis
    group_by_analysis_ratio(loan_df=loan_df, group_by_col='total_acc', titles=['total_acc', 'number_of_loans'],
                            ratio_title='ratio_bad_loans_by_total_acc', number_of_loans_threshold=30,
                            plt_ratio_threshold=0.1, xtick_label=8)
    # total_acc greater than 75 have significant number of defaults while number of these loans is 5-10 so not
    # a significant sample to conclude.
    # On increasing the sample size to 30 we do see a significant increase in defaults at extreme values.
    # For e.g. 5 credit lines has a delinquency of just greater than 20% which is 6% more than overall delinquency.


def group_by_analysis_ratio(loan_df, group_by_col, titles, ratio_title, number_of_loans_threshold,
                            plt_ratio_threshold, xtick_label):
    print(loan_df[group_by_col].unique())
    group_by_col_title = loan_df.groupby(group_by_col).size().reset_index()
    group_by_col_title.columns = titles
    group_by_col_title = group_by_col_title[group_by_col_title['number_of_loans'] >= number_of_loans_threshold]
    group_by_col_title_bad_loans = (loan_df.loc[loan_df['loan_status'] == 'Charged Off']) \
        .groupby(group_by_col).size().reset_index()
    group_by_col_title_bad_loans.columns = [group_by_col, 'number_of_bad_loans']

    col_wise = pd.merge(group_by_col_title, group_by_col_title_bad_loans, how='inner', on=group_by_col)
    col_wise[ratio_title] = \
        col_wise['number_of_bad_loans'] / col_wise['number_of_loans']
    col_wise = col_wise.loc[col_wise[ratio_title] >= plt_ratio_threshold]
    print_df(col_wise)
    # Plot
    plt.figure(figsize=(10, 15))
    plt.rcParams['xtick.labelsize'] = xtick_label
    sns.barplot(x=group_by_col, y=ratio_title, data=col_wise)
    plt.xticks(rotation=90)
    plt.show()
    plt.rcParams['xtick.labelsize'] = 8


def analyze_bi_variate(loan_df):
    print('analyze_bi_variate')
    # Converting loan status to categorical values
    loan_df = loan_df.loc[loan_df['loan_status'] != 'Current']
    loan_corr = loan_df.corr()
    loan_corr = loan_corr.drop(['Unnamed: 0', 'id', 'member_id'])
    loan_corr = loan_corr.drop(['Unnamed: 0', 'id', 'member_id'], axis=1)
    loan_corr.to_csv('loan_corr.csv')
    # Generate a mask for the upper triangle
    mask = np.zeros_like(loan_corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True
    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(11, 9))
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(loan_corr, mask=mask, cmap=cmap, vmax=.3, center=0,
                square=True, linewidths=.5, cbar_kws={"shrink": .5})
    plt.show()
    # Based on the correlation plot we can see that there is no significant correlation between items that we can
    # eliminate them except for mths_since_last_delinquency and delinq_2yrs.


def replace_values_emp_length(x):
    if x == '10+ years':
        return int(11)
    elif x == '< 1 year':
        return int(0)
    elif x == '1 year':
        return int(1)
    elif x is np.nan:
        return x
    else:
        return int(x.rstrip('years'))


def replace_values_int_rate(x):
    # 0, 0.03, 0.06, 0.09, ....
    return INT_RATE_RANGE[INT_RATE_RANGE.searchsorted(x, side='right')]


def replace_values_revol_bal(x):
    index = REVOL_BAL_RANGE.searchsorted(x, side='right')
    if 0 <= index < len(REVOL_BAL_RANGE):
        return REVOL_BAL_RANGE[index]
    return x


def replace_values_revol_util_rate(x):
    index = REVOL_UTIL_RANGE.searchsorted(x, side='right')
    if 0 <= index < len(REVOL_UTIL_RANGE):
        return REVOL_UTIL_RANGE[index]
    return x


def replace_values_dti(x):
    return DTI_RANGE[DTI_RANGE.searchsorted(x, side='right')]


def replace_values_total_acc(x):
    return TOTAL_ACC_RANGE[TOTAL_ACC_RANGE.searchsorted(x, side='right')]


def print_df(x):
    pd.set_option('display.max_rows', len(x))
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 2000)
    pd.set_option('display.float_format', '{:20,.2f}'.format)
    pd.set_option('display.max_colwidth', -1)
    print(x)
    pd.reset_option('display.max_rows')
    pd.reset_option('display.max_columns')
    pd.reset_option('display.width')
    pd.reset_option('display.float_format')
    pd.reset_option('display.max_colwidth')


def main():
    loan_df = read_loan_data()
    clean_loan_data(loan_df)
    loan_df = read_loan_clean_data()
    loan_df = process_data(loan_df)
    analyze_univariate(loan_df=loan_df)
    analyze_bi_variate(loan_df=loan_df)


if __name__ == '__main__':
    main()
