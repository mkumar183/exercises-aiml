simple linear regression 
straight line 
X and Y -> Independent and Dependent (Predictable) 
RSS 
R-square 
Beta0 and Beta1
Why R-square values are between 0 and 1 
TSS - worst possible line you can draw 

Simple Linear Regression in Python: 
Assumptions of SLR
Let’s take a look at what the assumptions of simple linear regression were:
1. Linear relationship between X and Y
2. Error terms are normally distributed (not X, Y)
3. Error terms are independent of each other
4. Error terms have constant variance (homoscedasticity)

Residual -> Predicted minus Actual (Train and Test datasets) 
Should be uniform distribution 
Residual Analysis: Ideally evenly distributed around 0 and no specific pattern. 
Error terms (Y) against predicted value (X)

Hypothesis Testing of B0 and B1: 
H0 = B1=0 
HA = B1!= 0 
p value of coefficient should be near 0 to indicate that alternate hypothesis can be accepted reasonably well. 

Multiple Linear Regression: 
Adding more variables helps 
same assumptions and inference

more isn't always helpful 
Multicollinearity 
Correlation and VIF are methods of detecting multicollinearity 

creating dummy variables for handling categorical values 








